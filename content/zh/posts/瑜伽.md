---
author: BauerDin
title: 瑜伽
date: 2019-12-17T12:00:06+09:00
description: 瑜伽
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 瑜伽
---
一提起瑜伽，大多数人会想到一些很难的身体姿势，但这些体式不能代表瑜伽， 瑜伽的意思是合一。

你称为"我自己"的，是你的身体，心智和能量。能量不容易体验到，但可以想到支持身体，心智运作的是能量。这是你能够下功夫的三个实相: 身体，心智和能量。 瑜伽就是校准这三个方面，使其合一的科学。

瑜伽可以根据你的意愿创建内在环境。当你调整到内在和谐的时候，你最佳状态就会发挥出来。

瑜伽不是让你成为超人，而是让你意识到做为人超级棒。
