---
author: BauerDin
title: 《道德经》第六十九章
date: 2020-03-08T12:00:06+09:00
description: 用兵有言：吾不敢为主，而为客；不敢进寸，而退尺。是谓行无行；攘无臂；扔无敌；执无兵。祸莫大于轻敌，轻敌几丧吾宝。故抗兵相若，哀者胜矣。
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 道
- 名
- 众妙之门
categories:
- 道家
series:
- 道德经
---
>**用兵有言：吾不敢为主，而为客；不敢进寸，而退尺。是谓行无行；攘无臂；扔无敌；执无兵。祸莫大于轻敌，轻敌几丧吾宝。故抗兵相若，哀者胜矣。**
