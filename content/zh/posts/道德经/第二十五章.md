---
author: BauerDin
title: 《道德经》第二十五章
date: 2020-03-08T12:00:06+09:00
description: 有物混成,先天地生,寂兮寥兮,独立而不改,周行而不殆,可以为天下母.
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 道
- 名
- 众妙之门
categories:
- 道家
series:
- 道德经
---
>**有物混成,先天地生,寂兮寥兮,独立而不改,周行而不殆,可以为天下母.吾不知其名,字之曰道,强为之名曰大.大曰逝,逝曰远.远曰反.故道大,天大,地大,王亦大.域中有四大,而王处一焉.人法地,地法天,天法道,道法自然.**
