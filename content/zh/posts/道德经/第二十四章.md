---
author: BauerDin
title: 《道德经》第二十四章
date: 2020-03-08T12:00:06+09:00
description:  企者不立，跨者不行，自见者不明，自是者不彰。自伐者无功，自矜者不长。其在道也，曰余食赘行。物或恶之，故有道者不处也。
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 道
- 名
- 众妙之门
categories:
- 道家
series:
- 道德经
---
>**企者不立，跨者不行，自见者不明，自是者不彰。自伐者无功，自矜者不长。其在道也，曰余食赘行。物或恶之，故有道者不处也。**
