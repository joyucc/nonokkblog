---
author: BauerDin
title: 《道德经》第八十章
date: 2020-03-08T12:00:06+09:00
description: 小国寡民。使有什伯之器而不用；使民重死而不远徙。虽有舟舆，无所乘之，虽有甲兵，无所陈之。使民复结绳而用之。
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 道
- 名
- 众妙之门
categories:
- 道家
series:
- 道德经
---
>**小国寡民。使有什伯之器而不用；使民重死而不远徙。虽有舟舆，无所乘之，虽有甲兵，无所陈之。使民复结绳而用之。甘其食，美其服，安其居，乐其俗。邻国相望，鸡犬之声相闻，民至老死，不相往来。**
