---
author: BauerDin
title: 《道德经》第三十七章
date: 2020-03-08T12:00:06+09:00
description: 道常无为而无不为。侯王若能守之，万物将自化。化而欲作，吾将镇之以无名之朴。镇之以无名之朴，夫将不欲。不欲以静，天下将自正。
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 道
- 名
- 众妙之门
categories:
- 道家
series:
- 道德经
---
>**道常无为而无不为。侯王若能守之，万物将自化。化而欲作，吾将镇之以无名之朴。镇之以无名之朴，夫将不欲。不欲以静，天下将自正。**
