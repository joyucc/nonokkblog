---
author: BauerDin
title: 《道德经》第二十八章
date: 2020-03-08T12:00:06+09:00
description: 知其雄，守其雌，为天下溪。为天下溪，常德不离，复归于婴儿。知其白，守其辱，为天下谷。为天下谷，常德乃足，复归于朴。
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 道
- 名
- 众妙之门
categories:
- 道家
series:
- 道德经
---
>**知其雄，守其雌，为天下溪。为天下溪，常德不离，复归于婴儿。知其白，守其辱，为天下谷。为天下谷，常德乃足，复归于朴。知其白，守其黑，为天下式。为天下式，常德不忒，复归于无极。朴散则为器，圣人用之，则为官长，故大智不割。 **
