---
author: BauerDin
title: 《道德经》第七十九章
date: 2020-03-08T12:00:06+09:00
description: 和大怨，必有馀怨；报怨以德，安可以为善？是以圣人执左契，而不责于人。有德司契，无德司彻。天道无亲，常与善人。
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 道
- 名
- 众妙之门
categories:
- 道家
series:
- 道德经
---
>**和大怨，必有馀怨；报怨以德，安可以为善？是以圣人执左契，而不责于人。有德司契，无德司彻。天道无亲，常与善人。**
