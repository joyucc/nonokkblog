---
author: BauerDin
title: 《道德经》第三十章
date: 2020-03-08T12:00:06+09:00
description: 以道佐人主者，不以兵强天下。其事好远。师之所处，荆棘生焉。大军之后，必有凶年。善有果而已，不以取强。
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 道
- 名
- 众妙之门
categories:
- 道家
series:
- 道德经
---
>**以道佐人主者，不以兵强天下。其事好远。师之所处，荆棘生焉。大军之后，必有凶年。善有果而已，不以取强。果而勿矜，果而勿伐，果而勿骄。果而不得已，果而勿强。物壮则老，是谓不道，不道早已。**
