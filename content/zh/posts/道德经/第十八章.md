---
author: BauerDin
title: 《道德经》第十八章
date: 2020-03-08T12:00:06+09:00
description: 大道废，有仁义；智慧出，有大伪；六亲不和，有孝慈；国家昏乱，有忠臣。
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 道
- 名
- 众妙之门
categories:
- 道家
series:
- 道德经
---
>**大道废，有仁义；智慧出，有大伪；六亲不和，有孝慈；国家昏乱，有忠臣。**
