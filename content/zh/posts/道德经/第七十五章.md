---
author: BauerDin
title: 《道德经》第七十五章
date: 2020-03-08T12:00:06+09:00
description: 民之饥，以其上食税之多，是以饥。民之难治，以其上之有为，是以难治。民之轻死，以其上求生之厚，是以轻死。夫唯无以生为者，是贤于贵生。
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 道
- 名
- 众妙之门
categories:
- 道家
series:
- 道德经
---
>**民之饥，以其上食税之多，是以饥。民之难治，以其上之有为，是以难治。民之轻死，以其上求生之厚，是以轻死。夫唯无以生为者，是贤于贵生。**
