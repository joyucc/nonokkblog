---
author: BauerDin
title: 《道德经》第十五章
date: 2020-03-08T12:00:06+09:00
description: 古之善为士者，微妙玄通，深不可识。
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 道
- 名
- 众妙之门
categories:
- 道家
series:
- 道德经
---
>**古之善为士者，微妙玄通，深不可识。夫惟不可识，故强为之容。豫兮若冬涉川，犹兮若畏四邻，俨兮其若客，涣兮若冰之将释，孰兮其若朴，旷兮其若谷，浑兮其若浊。孰能浊以澄静之徐清？孰能安以久动之徐生？保此道者不欲盈，夫惟不盈，故能敝不新成。**
