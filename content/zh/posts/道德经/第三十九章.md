---
author: BauerDin
title: 《道德经》第三十九章
date: 2020-03-08T12:00:06+09:00
description: 昔之得一者：天一以清，地得一以灵，神得一以宁，谷得一以盈，万物得一以生，侯王得一以为天下贞，其致之一也。
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 道
- 名
- 众妙之门
categories:
- 道家
series:
- 道德经
---
>**昔之得一者：天一以清，地得一以灵，神得一以宁，谷得一以盈，万物得一以生，侯王得一以为天下贞，其致之一也。天无以清，将恐裂，地无以宁，将恐发；神无以灵，将恐歇；谷无以盈，将恐竭；万物无以生。将死灭；侯王无以贞贵高，将死蹙。故贵以贱为本，高以下为基。侯王自谓孤、寡不　。此其以贱为本也？非乎？故致数车无车。不欲 如玉落落如石。**
