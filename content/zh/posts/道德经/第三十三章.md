---
author: BauerDin
title: 《道德经》第三十三章
date: 2020-03-08T12:00:06+09:00
description: 知人者智，自知者明。胜人者有力，自胜者强。知足者富。强行者有志。不失其所者久。死而不亡者寿。
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 道
- 名
- 众妙之门
categories:
- 道家
series:
- 道德经
---
>**知人者智，自知者明。胜人者有力，自胜者强。知足者富。强行者有志。不失其所者久。死而不亡者寿。**
