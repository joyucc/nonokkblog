---
author: BauerDin
title: 《道德经》第六十四章
date: 2020-03-08T12:00:06+09:00
description: 其安易持，其未兆易谋。其脆易泮，其微易散。为之于未有，治之于未乱。
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 道
- 名
- 众妙之门
categories:
- 道家
series:
- 道德经
---
>**其安易持，其未兆易谋。其脆易泮，其微易散。为之于未有，治之于未乱。合抱之木，生于毫末；九层之台，起于垒土；千里之行，始于足下。为者败之，执者失之。是以圣人无为，故无败，无执，故无失。民之从事，常于几成而败之。不慎终也。慎终如始，则无败事。是以圣人欲不欲，不贵难得之货；学不学，复众人之所过。以辅万物之自然，而不敢为。**
