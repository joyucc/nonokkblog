---
author: BauerDin
title: 快乐
date: 2019-12-17T12:00:06+09:00
description: 世界是一体的！
draft: true
hideToc: false
enableToc: true
enableTocContent: false
author: BauerDin
authorEmoji: 👽
tags: 
- 快乐
---

什么时候你感觉很好？在你开心快乐的时候，对不对？即使在你生病的时
候，如果你是快乐的，你仍感觉很好。因此，幸福从本质上来说指的是你内在感觉愉悦。如果你的身体感
觉舒服，我们称其为健康；如果你的身体感觉非常舒服，我们将其称为愉快。如果你的头脑感觉愉悦，这
叫宁静；如果它非常愉悦，这叫快乐。如果你的情感愉悦，这叫爱；如果它非常愉悦，这叫慈悲。如果你
的生命能量愉悦了，这叫极乐；如果它非常愉悦，就叫狂喜。